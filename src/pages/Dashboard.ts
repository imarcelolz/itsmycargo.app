import _ from "lodash";
import Fuse from "fuse.js";
import { Component, Prop, Vue } from "vue-property-decorator";
import { ShipmentProvider, Shipment } from "../providers/shipment-provider";

import DashboardTile from "../components/DashboardTile.vue";
import ShipmentsFilter from "../components/ShipmentsFilter.vue";
import ShipmentsList from "../components/ShipmentsList.vue";

import ShipmentDetails from "../components/ShipmentDetails.vue";
import ShipmentStatus from "../components/ShipmentStatus.vue";

@Component({
  components: {
    DashboardTile,
    ShipmentsList,
    ShipmentsFilter,
    ShipmentDetails,
    ShipmentStatus,
  }
})
export default class DashboardPage extends Vue {

  provider: ShipmentProvider = new ShipmentProvider();

  shipment: Shipment = <Shipment>{};
  shipments: Array<Shipment> = new Array<Shipment>();
  allShipments: Array<Shipment> = new Array<Shipment>();

  amountBooked: number = 0;
  amountConfirmed: number = 0;
  amountFinished: number = 0;

  filterVisible: boolean = false;
  filter: any = {
    text: '',
    status: 'booked',
    type: ''
  };

  // fuseSearchOptions = {
  //   shouldSort: true,
  //   threshold: 0.6,
  //   location: 0,
  //   distance: 100,
  //   maxPatternLength: 32,
  //   minMatchCharLength: 1,
  //   keys: [
  //     "reference",
  //     "origin",
  //     "destination",
  //   ]
  // };

  mounted() {
    this.provider.getShipments().then((shipments) => {
      this.allShipments = shipments;

      this.updateTiles();
      this.applyFilter();
    });

  }

  applyFilter() {
    let filter: any = {};

    this.shipments = this.allShipments;

    // if (this.filter.text != '') {
    //   this.shipments = this.fuseSearch.search(this.filter.text);
    // }

    if (this.filter.status != '') {
      filter.status = this.filter.status;
    }

    if (this.filter.type != '') {
      filter.cargo_type = this.filter.type;
    }

    this.shipments = _.filter(this.shipments, filter);
  }

  onShipmentChange(shipment: Shipment) {
    this.shipment = shipment;
  }

  updateTiles() {
    let grouped = _.countBy(this.allShipments, "status");

    this.amountBooked = grouped["booked"];
    this.amountConfirmed = grouped["confirmed"];
    this.amountFinished = grouped["finished"];
  }
}