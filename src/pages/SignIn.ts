import { Component, Prop, Vue } from "vue-property-decorator";

import AuthProvider from "../providers/auth-provider";

@Component
export default class SignInPage extends Vue {
  auth: AuthProvider = new AuthProvider();

  email: string = "";
  password: string = "";

  loading: boolean = false;
  errorMessage: string = "";

  onSignInClick() {
    this.loading = true;
    this.errorMessage = "";

    this.auth.signIn(this.email, this.password)
      .then(x => this.$emit("sign-in"))
      .catch(x => this.errorMessage = "Invalid email or password")
      .finally(() => this.loading = false);
  }

  submitEnabled() {
    if (!this.email || this.email.length == 0)
      return false;

    if (!this.password || this.password.length == 0)
      return false;

    return true;
  }
}