import { Vue } from "vue-property-decorator";

Vue.filter('uppercase', (value: string): string => {
  if (!value) { return ''; }

  return value.toUpperCase();
});
