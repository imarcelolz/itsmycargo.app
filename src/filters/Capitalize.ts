import { Vue } from "vue-property-decorator";

Vue.filter('capitalize', (value: string): string => {
  if (!value) { return ''; }

  const [head, ...tail] = value;
  return head.toUpperCase() + tail.join('');
});
