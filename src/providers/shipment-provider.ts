import Axios, { AxiosResponse } from "axios";

import { Envirolment } from "../envirolment";
import OfflineShipments from "../OfflineShipments";

export interface CargoUnit {
  id: number,
  shipment_id: number,
  payload: number,
  dimension_x: number,
  dimension_y: number,
  dimension_z: number,
  stackable: boolean,
  dangerous: boolean,
  created_at: Date | string,
  updated_at: Date | string
};

export interface Shipment {
  id: number,
  reference: string,
  cargo_type: string,
  status: string,
  origin: string,
  destination: string,
  planned_etd: Date | string,
  planned_eta: Date | string,
  cargo_units: Array<CargoUnit>
}

export class ShipmentProvider {
  private baseUrl: string;

  constructor() {
    this.baseUrl = Envirolment.webserivce + '/shipments';
  }

  public getShipmentsOnline(): Promise<Shipment[]> {

    const resolved = (res: AxiosResponse): Shipment[] => {

      console.debug("Shipments loaded!!", res);
      return <Shipment[]>res.data;
    };

    const error = (res: AxiosResponse): Shipment[] => {
      console.error("Error while loading shipments", res);

      return new Array<Shipment>();
    };

    return Axios.get(this.baseUrl)
      .then(resolved, error);
  }

  public getShipments(): Promise<Shipment[]> {
    const shipments = <Shipment[]>OfflineShipments;

    return Promise.resolve(shipments);
  }
}

