import Axios, { AxiosResponse, AxiosError } from "axios";

import { Envirolment } from "../envirolment";

interface SignInResponse {
  access_token: string
  token_type: string
  expires_in: number
  created_at: number
}

export default class AuthProvider {

  private baseUrl: string;

  constructor() {
    this.baseUrl = Envirolment.webserivce + '/oauth';
  }

  private set AuthToken(authToken: string) {
    Axios.defaults.headers.common['Authorization'] = authToken;
  }

  private get AuthToken(): string {
    return Axios.defaults.headers.common['Authorization'];
  }

  public signIn(email: string, password: string): Promise<string> {
    const url = this.baseUrl + '/token';

    const params = {
      grant_type: 'password',
      email: email,
      password: password
    };

    const resolved = (res: AxiosResponse): string => {
      const response = <SignInResponse>res.data;

      this.AuthToken = `${response.token_type} ${response.access_token}`;

      return 'Athenticated!';
    };

    return Axios.post(url, params)
      .then(resolved, this.errorHandler);
  }

  public signOut(): Promise<string> {
    const url = this.baseUrl + '/signout';

    const resolved = (res: AxiosResponse): string => {
      this.AuthToken = '';

      console.debug(res);

      return 'ok';
    };

    return Axios.delete(url)
      .then(resolved, this.errorHandler);
  }

  errorHandler(res: AxiosError): string {
    console.debug(res);

    throw 'Error';
  }
}