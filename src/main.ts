import Vue from 'vue'
import './plugins/fontawesome'
import App from './App.vue'
import AuthProvider from './providers/auth-provider';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

