import { Component, Prop, Vue } from "vue-property-decorator";
import { Shipment } from "../providers/shipment-provider";

import ShipmentStatus from "./ShipmentStatus.vue";

@Component({
  components: {
    ShipmentStatus
  }
})
export default class ShipmentsList extends Vue {
  seletedId: number = -1;
  @Prop() value!: Array<Shipment>;

  onItemClick(item: Shipment) {
    this.seletedId = item.id;
    this.$emit('change', item);
  }
}
