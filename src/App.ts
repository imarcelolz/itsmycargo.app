import * as _ from "lodash";
import { Component, Vue } from "vue-property-decorator";

import DashboardPage from "./pages/Dashboard.vue";
import SignInPage from "./pages/SignIn.vue";

import AuthProvider from "./providers/auth-provider";

import "./filters/Capitalize";
import "./filters/Uppercase";

@Component({
  components: {
    DashboardPage,
    SignInPage
  }
})
export default class App extends Vue {
  auth: AuthProvider = new AuthProvider();
  authenticated: boolean = false;

  onSignIn() {
    this.authenticated = true;
  }

  onSignOut() {
    this.auth.signOut().then(x => {
      this.authenticated = false;
    })
  }
}